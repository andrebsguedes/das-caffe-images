# DAS Caffe Images

Image similarity search WebApp developed in Django!



## Requirements

To run some dependencies are required:

- Have Caffe installed and the `$CAFFE_ROOT` environment variable set
- Have OpenCV3 installed with the python bindings
- Save all images in the folder `caffeimages/k_nearest/data/images` and have all images listed in the `caffeimages/k_nearest/data/val.txt` file
- Have `pip` and `python-virtualenv` installed

The python dependencies will be installed upon running the `./setup.sh` script.

## Run

After running the script, activate the `virtualenv` by executing:

```sh
$ source .env/bin/activate
```

To generate de LMDB from the input images enter the `caffeimages/k_nearest` folder and run:

```sh
$ ./create_imagenet.sh
```

To run the WebApp, enter the `caffeimages` folder and run:

```sh
$ python manage.py runserver
```

OBS: The app is configured to run on `gpu_mode`. To change modes edit the `caffeimages/k_nearest/worker.py` file replacing `gpu` with `cpu`.